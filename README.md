# DESCRIPTION:

## Story

John is a road worker, his job is digging holes for street lamp. For example: A section of 300 meters on the road need to be spaced 50 meters to install a street lamp, John needs to dig 7 holes:

    h.....h.....h.....h.....h.....h.....h

    h--->hole

John was tired, but to his dismay, the manager thought that the distance between the street lights was too large and they wanted to shorten the distance. There are two new plans: the distance of 30 meters, the distance of 25 meters. Which plan would John choose? John think seriously:

    h---> the old holes can be used  
    n---> new hole 
    b---> the old holes need buried

    the distance of 30 meters:
    h...n.bn...nb.n...h..n..bn...nb.n...h
    He needs to dig 8 holes and buried 4 old holes

    the distance of 25 meters:
    h..n..h..n..h..n..h..n..h..n..h..n..h
    He needs to dig 6 new holes, don't need to bury the old hole

Dig and bury both needs John to do. So it is clear that John will choose the plan of the distance of 25 meters.

## Task

Complete function `johnChoose()` that accepts 3 arguments:

- `length`: A number that is the length of road
- `oldd`: A number that is the old distance
- `newd`: An array that contains some new distance

Returns the new distance that John will choose. If more than one plan has the same amount of work, choose the one to return that the value of the index is small.

Please note:

1. If the new distance is more than or equals to the old distance, such distance is not in line with the requirements of the managers. So, If there is not an effective distance, please return `null`/`None`.
2. If the length of the road is not a multiple of the distance, John does not need to dig in the end of the road. For example: If the length is `100` and the distance is `30`, John need dig at the place of `0,30,60,90` meters, do not need to dig at the place of `100` meters.
3. Please pay attention to performance ;-)

## Example

    johnChoose(300, 50, Seq(30, 25)) == 25
    johnChoose(600, 60, Seq(50, 40, 30, 25, 20)) == 30
    johnChoose(600, 60, Seq(50, 40, 25, 20)) == 40
