object TTT50 {
  def johnChoose(length: Int, oldDistance: Int, newDistances: Seq[Int]): Option[Int] = {
    
    val oldHoles = (0 to length by oldDistance)
    
    val costs = for {
      d <- newDistances 
      if d < oldDistance
    } yield {
      val overlap = oldHoles.count(_%d == 0)
      (d, length/d + length - 2*overlap)
    }
    
    if (costs.nonEmpty) Some(costs.minBy(_._2)._1)
    else None
     
  }
}